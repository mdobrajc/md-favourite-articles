<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavArticles extends Model
{
    //define which columns can be set
    protected $fillable = ['title', 'section_name', 'image'];
    
    public function user(){
    	return $this->belongsTo('App\User');
    }
}
