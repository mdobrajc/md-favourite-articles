<?php

namespace App;

use Illuminate\Support\Facades\Auth;

class PasswordVerifier
{
	//we use this function to authenticate through Oauth with our local credentials
	public function verify($username, $password){
		$credentials = [
			'email'		=> $username,
			'password'	=> $password
		];

		if(Auth::once($credentials)){
			return Auth::user()->id;
		}

		return false;
	}
}