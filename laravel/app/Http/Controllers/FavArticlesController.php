<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\FavArticles;
use App\User;
use LucaDegasperi\OAuth2Server\Authorizer;
use Response;
use Validator;

class FavArticlesController extends Controller
{

    private $authorizer;

    public function __construct(Authorizer $authorizer)
    {
        $this->authorizer = $authorizer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get user id from oauth
        $user_id = $this->authorizer->getResourceOwnerId();
        //find our favourited articles
        $articles = FavArticles::where('user_id', $user_id)->orderBy('created_at', 'desc')->take(20)->get()->toArray();
        
        //return them
        return Response::json([
                'success' => true,
                'data' => $articles,
            ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //setting up the validator
        $validator = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'section_name' => 'required|max:255',
                'image' => 'required'
            ]);

        //check if we got all our required fields
        //if not, send error
        if($validator->fails()){
            return Response::json([
                    'success' => false,
                    'message' => 'Validation failed'
                ], 500);
        }

        //get user from oath
        $user_id = $this->authorizer->getResourceOwnerId();
        //create new article and insert data from request
        $article = new FavArticles;
        $article->title = $request->input('title');
        $article->section_name = $request->input('section_name');
        $article->image = $request->input('image');
        $article->user_id = $user_id;

        //save it
        $article->save();

        //tell the client that we successfully saved it
        return Response::json([
                'success' => true,
                'message' => 'Article favourited'
            ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //get user id...
        $user_id = $this->authorizer->getResourceOwnerId();
        //find article by id and check user id, just so we won't delete other users favourites
        $article = FavArticles::where('id', $id)->where('user_id', $user_id)->first();

        //check if we found an article, if we did, delete
        //respond to client depending on the outcome
        if($article){
            $article->delete();
            return Response::json([
                    'success' => true,
                    'message' => 'Article successfully deleted'
                ], 200);
        } else {
            return Response::json([
                    'success' => false,
                    'message' => 'No matching article'
                ], 200);
        }
    }
}
