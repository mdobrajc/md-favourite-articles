<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('oauth/access_token', ['middleware' => 'cors', function() {
	return Response::json(Authorizer::issueAccessToken());
}]);

Route::group(['prefix' => 'api/v1/', 'middleware' => ['oauth', 'cors']], function(){
	Route::get('articles', 'FavArticlesController@index');
	Route::post('articles', 'FavArticlesController@store');
	Route::delete('articles/{id}', 'FavArticlesController@destroy');
});