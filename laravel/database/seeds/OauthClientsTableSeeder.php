<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class OauthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            'id' => '48ada516b887f55fa780d209',
            'secret' => '351019bf07364a3eede573d7643589fc',
            'name' => 'NG App'
        ]);
    }
}
