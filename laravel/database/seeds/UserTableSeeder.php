<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Test',
            'email' => 'test@test.si',
            'password' => bcrypt('test'),
        ]);

        DB::table('users')->insert([
            'name' => 'Marcel',
            'email' => 'md@md.si',
            'password' => bcrypt('test'),
        ]);
    }
}
