# md-favourite-articles
Made by the rules of [Media24 Task](http://media24.si/task.html)

## Setup Instructions
Clone the repository

```
#!

git clone https://bitbucket.org/mdobrajc/md-favourite-articles
```

Download dependencies in Laravel folder

```
#!

composer install
```

Rename .env.example to .env, adjust the values and generate a key

```
#!

php artisan key:generate
```

Migrate and seed the database with PHP Artisan

```
#!

php artisan migrate
php artisan db:seed
```

Run the PHP server

```
#!

php artisan serve --port=8080
```

Move to the ng folder, and download dependencies

```
#!

npm install
bower install
```

Change the artApiUrl in services/article.js and run the client

```
#!

grunt serve
```

Login

```
#!

Username: test@test.si || md@md.si
Password: test
```


## Contributor
Marcel Dobrajc