'use strict';

describe('Filter: artfilter', function () {

  // load the filter's module
  beforeEach(module('mdFavArtNgApp'));

  // initialize a new instance of the filter before each test
  var artfilter;
  beforeEach(inject(function ($filter) {
    artfilter = $filter('artfilter');
  }));

  it('should return the input prefixed with "artfilter filter:"', function () {
    var text = 'angularjs';
    expect(artfilter(text)).toBe('artfilter filter: ' + text);
  });

});
