'use strict';

/**
 * @ngdoc overview
 * @name mdFavArtNgApp
 * @description
 * # mdFavArtNgApp
 *
 * Main module of the application.
 */
angular
.module('mdFavArtNgApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
])
.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
          templateUrl: 'views/articles.html',
          controller: 'ArticleCtrl',
          controllerAs: 'ctrl',
          activetab: 'article'
      })
      .when('/login', {
          templateUrl: 'views/login.html',
          controller: 'UserCtrl',
          controllerAs: 'ctrl',
          activetab: 'login'
      })
      .when('/favart', {
          templateUrl: 'views/favarticles.html',
          controller: 'ArticleCtrl',
          controllerAs: 'ctrl',
          activetab: 'favart'
      })
      .otherwise({
          redirectTo: '/'
      });
})