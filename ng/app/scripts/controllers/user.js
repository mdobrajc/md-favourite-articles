'use strict';

/**
 * @ngdoc function
 * @name mdFavArtNgApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the mdFavArtNgApp
 */
angular.module('mdFavArtNgApp')
.controller('UserCtrl', ['$scope', '$rootScope', '$window', 'OauthService', function ($scope, $rootScope, $window, OauthService) {
    //redirect if we're already logged in
    if(!OauthService.isExpired()){
        $scope.$parent.newStatus = true;
        $window.location.href = '#/';
        return;
    }

    $scope.user = {};
    //Maybe use this instead of $scope?
    $scope.grant_type = OauthService.grant_type;
    $scope.client_id = OauthService.client_id;
    $scope.client_secret = OauthService.client_secret;
    //
    $scope.success = true;
    $scope.error_msg = '';

    this.Login = function Login(user){
        //try to login
        OauthService.Login(user, function (response){
            if(response.success){
                //control flag for navigation bar
                $scope.$root.newStatus = true;
                //redirect
                $window.location.href = '#/';
            }else{
                $scope.success = false;
                $scope.error_msg = response.message;
            }
        });
    }
}]);
