'use strict';

/**
 * @ngdoc function
 * @name mdFavArtNgApp.controller:ArticleCtrl
 * @description
 * # ArticleCtrl
 * Controller of the mdFavArtNgApp
 */
angular.module('mdFavArtNgApp')
.controller('ArticleCtrl', ['$scope', '$window', '$location', 'OauthService', 'ArticleService', function ($scope, $window, $location, OauthService, ArticleService) {
    
    //redirect to login
    if(OauthService.isExpired()){
        $scope.$parent.newStatus = false;
        $window.location.href = '#/login';
        return;
    }

    $scope.articles = [];
    $scope.glyph = [];

    var path = $location.path();

    //to decide on which path we are
    //maybe I should've created a separate controller
    if(path === '/'){
        ArticleService.getArticle(function (response){
            response.data.forEach(function(article, i){
                //only push the important stuff
                $scope.articles.push({index: i, title: article.title, section_name: article.section_name, image: article.image});
            })
        });
    }else if(path === '/favart'){
        ArticleService.getFavourites(function (response){
            response.data.forEach(function(article, i){
                //only push the important stuff
                $scope.articles.push({index: i, id: article.id, title: article.title, section_name: article.section_name, image: article.image});
            });
        });
    }

    $scope.showImg = [0, 1, 2, 3, 4];


    $scope.favArticle = function (article){
        
        ArticleService.favouriteArticle(article, function (response){
            if(response.success){
                //change the glyph to a full star
                $scope.glyph[article.index] = true;
            }
        });
    }

    $scope.deleteArticle = function (article){

        ArticleService.removeFavourite(article.id, function (response){
            if(response.success){
                //get the index of the article for future use
                //splice array
                var del_index = article.index;
                $scope.articles.splice(del_index, 1);

                //in case there's less than 5 articles
                //we just need to remove the last index in the show array
                if($scope.articles.length < 5){
                    $scope.showImg.splice($scope.showImg.length-1, 1);
                }

                //check if the first index isn't zero, because if it is, and we split the array
                //the first index will go into negatives, and we don't want that
                //if the last shown article has a greater value than the new length of articles array
                //then we just need to readjust the indexes of shown
                if($scope.showImg[0] != 0){
                    if($scope.showImg[$scope.showImg.length-1] >= $scope.articles.length-1){
                        for(var i=0; i < $scope.showImg.length; i++){
                            $scope.showImg[i]--;         
                        }
                    }
                }

                //if everything goes alright, and we delete an article when the shown indexes are
                //5 6 7 8 9, while the articles array is of the length 20
                //then readjusting the indexes is enough, and we don't need to change anything

                //readjusting indexes of the articles array
                //we start from the index which we deleted
                //because that's where the gap is created
                for(var i=del_index; i < $scope.articles.length; i++){
                    $scope.articles[i].index--;
                }
            }
        });  
    }

    $scope.show = function (index){
        //show the favourited article
        if($scope.showImg.indexOf(index) !== -1){
            return true;
        }
        return false;
    }

    $scope.showClass = function (index){
        //set the class
        if($scope.showImg.indexOf(index) !== -1){
            return 'col-xs-16 animate-img';
        }
        return '';
    }

    $scope.sliderForward = function(){
        //check at what article we are, if we're at the end, make the button useless
        if($scope.showImg[$scope.showImg.length-1] >= $scope.articles.length-1){
            return;
        }

        //increase indexes, moving the thing forward
        for(var i=$scope.showImg.length-1; i >= 0; i--){
            $scope.showImg[i]++;         
        }
    }

    $scope.sliderBack = function(){
        //check if we're at the beginning, if we're, make the button useless
        if($scope.showImg[0] <= 0){
            return;
        }

        //reduce indexes, moving the thing backwards
        for(var i=0; i < $scope.showImg.length; i++){
            $scope.showImg[i]--;         
        }
    }

}]);
