'use strict';

/**
 * @ngdoc function
 * @name mdFavArtNgApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the mdFavArtNgApp
 */
angular.module('mdFavArtNgApp')
.controller('NavCtrl',[ '$scope', '$rootScope', '$route', '$window', 'OauthService', function ($scope, $rootScope, $route, $window, OauthService) {
    
    //flag for navigation
    $scope.isLogged = !OauthService.isExpired();
    $rootScope.newStatus = $scope.isLogged;
    //for class changes
    $scope.$route = $route;

    //watch for status change upon login
    $rootScope.$watch('newStatus', function(newVal, oldVal){
        $scope.isLogged = newVal;
    });

    $scope.Logout = function(){
        OauthService.Logout(function(response){
            if(response.success){
                $rootScope.newStatus = false;
                $window.location.href = '#/login';
            }else{
                alert(response.message);
            }
        });
    }
}]);
