'use strict';

/**
 * @ngdoc filter
 * @name mdFavArtNgApp.filter:artfilter
 * @function
 * @description
 * # artfilter
 * Filter in the mdFavArtNgApp.
 */
angular.module('mdFavArtNgApp')
.filter('imagefilter', function () {
    return function (input) {
        var temp = input.replace('##WIDTH##', '237');
        return temp.replace('##HEIGHT##', '237');      
    };
})
.filter('sectionfilter', function () {
    return function (input) {
      var split = input.split(' / ');
      return split[split.length-1];
    };
})
.filter('labelfilter', function () {
    return function (input) {
      var split = input.split(' / ');
      return split[0].toLowerCase();
    };
})