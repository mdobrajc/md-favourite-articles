'use strict';

/**
 * @ngdoc service
 * @name mdFavArtNgApp.Oauth
 * @description
 * # Oauth
 * Service in the mdFavArtNgApp.
 */
angular.module('mdFavArtNgApp')
.service('OauthService', ['$http', '$window', function ($http, $window) {
    //API url
    var apiUrl = 'http://localhost:8080/';
    //Oauth2 parameters
    this.grant_type = 'password';
    this.client_id = '48ada516b887f55fa780d209';
    this.client_secret = '351019bf07364a3eede573d7643589fc';
    var access_token = $window.localStorage['access_token'] || null;
    var expires = $window.localStorage['expires'] || null;

    this.Login = function(user, callback){
        //check if we're already logged
        if(!this.isExpired()){
            callback({
                success: false,
                message: 'You are already logged in.'
            });
            return;
        }

        $http.post(apiUrl + 'oauth/access_token', user).then(function (response){
            //save access_token
            access_token = response.data.access_token;
            $window.localStorage['access_token'] = access_token;
            
            //save expiration date      
            var now = new Date();
            now.setSeconds(now.getSeconds() + response.data.expires_in);
            expires = now.getTime();
            $window.localStorage['expires'] = expires;

            callback({
                success: true,
                message: 'User successfully logged in.'
            });
            return;
        }, function (){
            callback({
                success: false,
                message: 'User authentication failed.'
            });
            return;
        });
    }

    this.Logout = function(callback){
        //check if there's a user present
        if(this.LoggedOff()){
            callback({
                success: false,
                message: 'There is no user to log out.'
            });
            return;
        }

        //incase we were storing something in localStorage, we should only remove the keys
        $window.localStorage.clear();
        access_token = null;
        expires = null;
        callback({
            success: true,
            message: 'User logged out.'
        });
    }

    this.LoggedOff = function(){
        //Turning it into a boolean
        return !access_token;
    }

    this.isExpired = function(){
        //return true if there's no user logged in
        if(this.LoggedOff()){
            return true;
        }
        
        //return true if the token has expired
        var now = new Date();
        if(now.getTime() > parseInt(expires)){
            //incase we were storing something in localStorage, we should only remove the keys
            $window.localStorage.clear();
            access_token = null;
            expires = null;
            return true;
        }
        
        //return false if a user is present with a valid token
        return false;
    }

    this.getToken = function(){
        return access_token;
    }
    
}]);
