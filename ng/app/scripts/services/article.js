'use strict';

/**
 * @ngdoc service
 * @name mdFavArtNgApp.Oauth
 * @description
 * # Oauth
 * Service in the mdFavArtNgApp.
 */
angular.module('mdFavArtNgApp')
.service('ArticleService', ['$http', '$window', 'OauthService', function ($http, $window, OauthService) {
    //API urls
    var myApiUrl = 'http://localhost:8080/api/v1/';
    var artApiUrl = 'some/api'; //removed
    //Article API pre-defined params
    var rid = 22,
        order = 'desc',
        limit = 20;

    this.getArticle = function(callback){
        //check if the token is valid
        if(OauthService.isExpired()){
             callback({
                success: false,
                message: 'You are not logged in.'
            });
            return;
        }

        //get articles
        $http.get(artApiUrl + 'articles', {params: {resource_id: rid, order: order, limit: limit}}).then(function (response){
            callback({
                success: true,
                data: response.data.data.list
            });
            return;            
        }, function (){
            callback({
                success: false,
                message: 'Article retrieval failed.'
            });
            return;
        });
    }

    this.favouriteArticle = function(article, callback){
        if(OauthService.isExpired()){
             callback({
                success: false,
                message: 'You are not logged in.'
            });
            return;
        }

        $http.post(myApiUrl + 'articles', {access_token: OauthService.getToken(), title: article.title, section_name: article.section_name, image: article.image}).then(function (response){
            callback({
                success: true,
                message: 'Article favourited.'
            });
            return;
        }, function (response){
            callback({
                success: false,
                message: 'An error has occurred.'
            });
            return;
        });
    }

    this.getFavourites = function(callback){
        if(OauthService.isExpired()){
             callback({
                success: false,
                message: 'You are not logged in.'
            });
            return;
        }

        $http.get(myApiUrl + 'articles', {params: {access_token: OauthService.getToken()}}).then(function (response){
            callback({
                success: true,
                data: response.data.data
            });
            return;            
        }, function (){
            callback({
                success: false,
                message: 'Favourites retrieval failed.'
            });
            return;
        });
    }

    this.removeFavourite = function(article_id, callback){
        if(OauthService.isExpired()){
             callback({
                success: false,
                message: 'You are not logged in.'
            });
            return;
        }

        $http.delete(myApiUrl + 'articles/' + article_id, {data: $.param({access_token: OauthService.getToken()}), headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(function (response){
            callback({
                success: true,
                message: 'Article deleted.'
            });
            return;            
        }, function (response){
            callback({
                success: false,
                message: 'An error has occurred.'
            });
            return; 
        });
    }
}]);
